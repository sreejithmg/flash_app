import 'package:flashapp/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flashapp/constants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatScreen extends StatefulWidget {
  static const String id = 'chat-screen';
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  String str = 'sreejith';
  final firestore = FirebaseFirestore.instance;
  // final firestor = Firestore.instance;
  final CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('message');
  final auth = FirebaseAuth.instance;
  var loggedInUser;
  var users;
  var messageText;
  @override
  void initState() {
    getCurrentUser();
    super.initState();
  }

  getCurrentUser() {
    try {
      final user = auth.currentUser;
      if (user != null) {
        // users = FirebaseUser;
        users = user;
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                auth.signOut();
                Navigator.pop(context);
              }),
        ],
        title: Text('⚡️Chat'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: kMessageContainerDecoration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      onChanged: (value) {
                        messageText = value;
                      },
                      decoration: kMessageTextFieldDecoration,
                    ),
                  ),
                  FloatingActionButton(
                    onPressed: () async {
                      await collectionReference
                          .add({'text': messageText, 'sender': loginemail});
                    },
                    child: Text(
                      'Send',
                      style: kSendButtonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
